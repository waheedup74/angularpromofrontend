import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendConnectorService {

  private baseUrl: string = "https://localhost/";

  constructor(private http: HttpClient) {

  }

  registerPackageRequest(packageData: any) {
    var promise = new Promise((resolve, reject) => {
      return this.http.post(this.baseUrl + 'registerProject', packageData).subscribe(
        (response: any) => {

        }
      );
    })
    return promise;
  }
  

   payPalCheckoutRequest(packagePrice: any) {
    console.log(packagePrice);
    var promise = new Promise((resolve, reject) => {
      return this.http.post('http://baseUrlHere/api/paypalapi', packagePrice).subscribe(
        (response: any) => {
          console.log(response);
        }
      );
    })
    return promise;
  }

}
